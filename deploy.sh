git archive HEAD -o production.zip
scp ./production.zip root@207.154.217.12:/var/www/asia-toefl-test.org/api.zip
ssh root@207.154.217.12 'cd /var/www/asia-toefl-test.org && unzip -o api.zip -d ./api && cd api && source venv/bin/activate && pip install -r requirements.txt && alembic upgrade head && deactivate && cd .. && rm ./api.zip && sudo systemctl restart asia-toefl-test-api'
