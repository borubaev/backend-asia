from sqlalchemy import String, Column, Integer

from utills.database import Base


class Admin(Base):
    __tablename__ = 'admins'

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String(255), nullable=False)
    password = Column(String(255), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'login': self.login,
        }
